package home;
/*
 * 
5、从控制台输入年份，计算母亲节？
(提示：母亲节为每年的5月第二个星期日)
 */
import java.util.Scanner;
import java.util.Calendar;
public class Untility_05 {

	public static void main(String[] args) {
		Calendar t=Calendar.getInstance();
		Scanner input=new Scanner(System.in);
		System.out.print("请输入年份：");
		int year=input.nextInt();
		t.set(Calendar.YEAR,year);
		t.set(Calendar.MONTH,4);
		t.set(Calendar.DAY_OF_WEEK,7);
		
		int n=0;
		if(t.get(Calendar.DAY_OF_MONTH)<=7)
		{
			n=7+t.get(Calendar.DAY_OF_MONTH);
		}else
		{
			n=t.get(Calendar.DAY_OF_MONTH);
		}
		
		System.out.println(year+"年母亲节是：\n"
				+t.get(Calendar.YEAR)+"年"
				+(t.get(Calendar.MONTH)+1)+"月"
				+n+"日"
				+"\n星期"+(t.get(Calendar.DAY_OF_WEEK)==7?"日":
					t.get(Calendar.DAY_OF_WEEK)==6?"六":
						t.get(Calendar.DAY_OF_WEEK))
				);

	}

}
