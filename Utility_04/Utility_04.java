package home;
import java.util.Arrays;
/*
 * 4、使用Java实现双色球功能，具体规则如下：
双色球规则红球33选6，蓝球16选1，不允许重复
使用Math类的random()方法在1到33内随机产生6个红球号码，每产生一个判断是否已经在数组中存在 ，不存在则保存于数组，否则重选
从1到16间随机产生一个蓝球号码。
 */
import java.util.Random;
public class Utility_04 {
	
	static int num[]=new int[6];
	
	public static boolean isFind(int n)
	{
		boolean isFind=true;
		for(int i=0;i<num.length;i++)
		{
			if(n==num[i])
			{
				isFind=false;
			}
		}
		return isFind;
	}

	public static void main(String[] args) {
		Random rand=new Random();
		
		for(int i=0;i<num.length;i++)
		{
			if(num[i]==0)
			{
				int n;
				boolean isFind = false;
				do {
				n=rand.nextInt(33)+1;
				isFind=isFind(n);
				}while(!isFind);
				num[i]=n;
			}
		}
		int num2=rand.nextInt(15)+1;
		System.out.println("双色球预测号:");
		for(int i=0;i<num.length;i++)
		{
			System.out.print(num[i]);
			if(i==num.length-1)
			{
				System.out.println(" + "+num2);
				break;
			}
			System.out.print(",");
		}
		
	    System.out.println("预祝您早日中大奖！");

	}

}
