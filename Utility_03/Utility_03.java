package home;

import java.util.ArrayList;
import java.util.Scanner;
public class Utility_03 {
	
	public static StringBuffer transform(double num)
	{
		StringBuffer ch=new StringBuffer(String.valueOf(num));
		for(int i=ch.lastIndexOf(".")-3;i>0;i=i-3)
		{
		   ch.insert(i, ",");
		}
		return ch;
	}

	public static void main(String[] args) {
		
		Scanner input=new Scanner(System.in);
		
		ArrayList<Product> proList=new ArrayList<Product>();
		proList.add(new Product(1,"电风扇",124.23));
		proList.add(new Product(2,"洗衣机",4500.0));
		proList.add(new Product(3,"电视机",8800.9));
		proList.add(new Product(4,"冰箱",5000.88));
		proList.add(new Product(5,"空调",4456.0));
		
		System.out.println("*****欢迎进入商品批发城***********");
		System.out.println("编号"+"\t"+"商品"+"\t"+"价格");
		for(int i=0;i<proList.size();i++)
		{
			System.out.println(proList.get(i).getId()
					+"\t"+proList.get(i).getName()
					+"\t"+transform(proList.get(i).getPrice()));
		}
		System.out.println("****************************");
		
		System.out.print("请输入您批发的商品编号：");
		int id=input.nextInt();
		System.out.print("请输入您批发数量：");
		int num=input.nextInt();
		System.out.println("您需要付款： "+transform(proList.get(id-1).getPrice()*num));
		
	}

}
