package home;
/*
 * 
 * 2、输入一个字符串，输入一个要查找的字符串，判断查找的字符串在字符串中出现的次数
        要求：查找的字符串随便输入，都可以查询出总数。
 */
import java.util.Scanner;
public class Utility_02 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		String ch1=new String();
		String ch2=new String();
		System.out.println("请输入一个字符串：");
		ch1=input.next();
		System.out.println("请输入要查找的字符：");
		ch2=input.next();
		int n=ch2.length();
				
		int num=0;
		
		for(int i=0;i<ch1.length()-n+1;i++)
		{
			if((ch1.substring(i, i+n)).equals(ch2))
			{
				num++;
			}
		}
		System.out.println(ch1+"中出现"+num+"次"+ch2);

	}

}
